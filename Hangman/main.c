//
//  main.c
//  Hangman
//
//  Created by Martin Haynes on 24/08/2014.
//  Copyright (c) 2014 DotMH. All rights reserved.
//

#include <stdio.h>
#include <string.h>

#define YES "yes"

struct Player {
    const char *name;
    int guesses;
    char guessed[256];
    int lives;
};

const int lives = 11;

void ask(char * message , char * use);
int yes(char * message);
void helpText( int guesses );
void playLetters( char * played);
int includeLetter( char * word , char * played);

int main(int argc, const char * argv[])
{
    char name[128];
    ask("Hello , what is your name?" , name);
    
    struct Player player = {name , 0 , "" , lives};
    
    char yes_question[1024];
    sprintf(yes_question, "Hello %s are you ready to begin?" , name);
    if (yes(yes_question)) {
        int gameOver = 0;
        helpText(lives);
        while ( !gameOver) {
            char guess[128];
            ask("Which letter do you want to play?" , guess);
        }
    }
    
    puts("Thanks for Playing");
    return 0;
}

void ask(char * message , char * use)
{
    printf("%s \n" , message);
    fgets(use , sizeof(use) , stdin);
    use[strlen(use)-1] = '\0';
}

int yes(char * message) {
    char answer[2];
    int result = 0;
    ask((strcat(message, " [yes]")) , answer);
    
    if (strcmp(answer, YES) == 0) {
        result = 1;
    }
    
    return result;
}

void helpText( int lives ) {
    printf("You have %i Lives in order to guess the word I am thinking off \n" , lives);
    puts("guess a letter at a time until you can guess the word \n");
    puts("Lets begin");
}

void playLetters(char * played) {
    
}

int includesLetter( char * word , char * played) {
    
    int result = 0;
    
    for (char * pt_word; pt_word; ++pt_word) {
        if (strcmp(pt_word, played) == 0) {
            result = 1;
            break;
        }
    }
    
    return result;
}